# Google Customer Reviews

Google Customer Reviews is a free service that enables Google to collect valuable feedback from customers who’ve made a purchase on your site.

This module works with commerce 2.

## About
You need a google merchant account with a url associate.
You will see the badge only in develop if you are working in local. You will see the badge with an error 404
## Installation

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229), use
Composer to download the module.

   ```sh
composer require "drupal/google_reviews"
   ```
```sh
drush en google_reviews
   ```
### Make it running

Just follow the route /admin/commerce/config/google_reviews and fill the form.


